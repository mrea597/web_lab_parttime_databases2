-- 1. Display the departments offering courses.
SELECT DISTINCT(dept) FROM unidb_courses;

-- 2. Display the semesters being attended.
SELECT DISTINCT(semester) FROM unidb_attend;

-- 3. Display the courses that are attended.
SELECT DISTINCT c.dept, c.num, c.descrip, c.coord_no
FROM unidb_courses AS c, unidb_attend AS a 
WHERE c.dept = a.dept AND c.num = a.num;

-- 4. List the students’ first names, last names, and country, ordered by first name.
SELECT fname, lname, country FROM unidb_students ORDER BY fname;

-- 5. List all lecturers, ordered by their office.
SELECT * FROM unidb_lecturers ORDER BY office;

-- 6. List all staff whose staff number is greater than 500.
SELECT * FROM unidb_lecturers WHERE staff_no > 500;

-- 7. List all students whose id is greater than 1668 and less than 1824.
SELECT id, fname, lname, country FROM unidb_students
WHERE id > 1668 AND id < 1824;
-- or:
SELECT id, fname, lname, country FROM unidb_students
WHERE id BETWEEN 1669 AND 1823;

-- 8. List the students from NZ, Australia and USA.
SELECT id, fname, lname, country FROM unidb_students
WHERE country IN ('NZ', 'AU', 'US'); 

-- 9. List all the lecturers in G block.
SELECT * FROM unidb_lecturers WHERE office LIKE ('G%');

-- 10. List the courses NOT from the computer science department.
SELECT * FROM unidb_courses WHERE NOT dept='comp';